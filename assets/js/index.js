$("#myTab .nav-item").click(function(e){
    e.preventDefault();
    e.stopPropagation();
});

$("#reset1").click(function(){
    $("#wecc").val("");
    $("#dir").val("");

    var form = $(".quantity-form")[0]
    form.classList.remove('was-validated');
});

$("#next1").click(function(event){
    var form = $(".quantity-form")[0]
    if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        form.classList.add('was-validated');
        return
    }
    $('#state-tab').tab('show');
});

$("#prev").click(function(){
    $('#quantity-tab').tab('show');
});

$("#reset2").click(function(){
    $("#area").val("");
    $("#population").val("");
    $("#gdp").val("");
    $("#industry_area").val("");
    $("#industry_tax").val("");
    $("#water_usage").val("");
    $("#arable_land_size").val("");
    $("#fertilizer_usage").val("");
    $("#industry_delta").val("");
    $("#industry_waste").val("");

    $("#s11").val("");
    $("#s12").val("");
    $("#s13").val("");
    $("#s21").val("");
    $("#s31").val("");
    $("#s32").val("");
    $("#s33").val("");
    $("#s41").val("");

    $("#r11").val("");
    $("#r21").val("");
    $("#r22").val("");
    $("#r31").val("");
    $("#r32").val("");
    $("#r41").val("");
    $("#r42").val("");

    $("#e11").val("");
    $("#e21").val("");

    var form = $(".state-form")[0]
    form.classList.remove('was-validated');
});

$("#submit").click(function(){
    var form = $(".state-form")[0]
    if (form.checkValidity() === false) {
        event.preventDefault();
        event.stopPropagation();
        form.classList.add('was-validated');
        return
    }
    $('#result-tab').tab('show');

    var wecc = $("#wecc").val();
    var dir = $("#dir").val();

    var sur_wecc = wecc - dir;
    console.log("承载力富余量:" + sur_wecc);

    var area = parseFloat($("#area").val());
    var population = parseFloat($("#population").val());
    var gdp = parseFloat($("#gdp").val());
    var industry_area = parseFloat($("#industry_area").val());
    var industry_tax = parseFloat($("#industry_tax").val());
    var water_usage = parseFloat($("#water_usage").val());
    var arable_land_size = parseFloat($("#arable_land_size").val());
    var fertilizer_usage = parseFloat($("#fertilizer_usage").val());
    var industry_delta = parseFloat($("#industry_delta").val());
    var industry_waste = parseFloat($("#industry_waste").val());

    var s11 = parseFloat($("#s11").val());
    var s12 = parseFloat($("#s12").val());
    var s13 = parseFloat($("#s13").val());
    var s21 = parseFloat($("#s21").val());
    var s31 = parseFloat($("#s31").val());
    var s32 = parseFloat($("#s32").val());
    var s33 = parseFloat($("#s33").val());
    var s41 = parseFloat($("#s41").val());

    var r11 = parseFloat($("#r11").val());
    var r21 = parseFloat($("#r21").val());
    var r22 = parseFloat($("#r22").val());
    var r31 = parseFloat($("#r31").val());
    var r32 = parseFloat($("#r32").val());
    var r41 = parseFloat($("#r41").val());
    var r42 = parseFloat($("#r42").val());

    var e11 = parseFloat($("#e11").val());
    var e21 = parseFloat($("#e21").val());

    var d1 = population / area;
    var d2 = gdp / area;
    var d3 = industry_tax / industry_area;
    
    var p1 = water_usage / (gdp * 10000);
    var p2 = fertilizer_usage / arable_land_size;
    var p3 = industry_waste / industry_delta;

    var s1 = 100 * s11 / (s12 * s13);
    var s2 = 100 * water_usage / s21;
    var s3 = s31;
    var s4 = 100 * s41 / area;

    var r1 = r11 / (population / 10000);
    var r2 = 100 * r22 / r21;
    var r3 = 100 * r32 / r31;
    var r4 = 100 * r42 / r41;

    var e1 = industry_tax * 10000 / e11;
    var e2 = industry_tax / e21;

    console.log("D1人口密度(人/km2):" + d1);
    console.log("D2单位国土面积地区生产总值(亿元/km2):" + d2);
    console.log("D3单位工业用地工业产出税收(万元/亩):" + d3);

    console.log("P1万元地区生产总值用水量(m3):" + p1);
    console.log("P2单位耕地面积化肥施用量（折纯）（kg/hm2）:" + p2);
    console.log("P3单位工业产值废水排放量（m3/104元）" + p3);

    console.log("S1区域出境断面水质达标率（%）:" + s1);
    console.log("S2水资源开发利用率（%）:" + s2);
    console.log("S3控制断面平均流量（%）:" + s3);
    console.log("S4区域林草覆盖率（%）:" + s4);

    console.log("R1水环境监管能力（万人环保专职人员数）(人):" + r1);
    console.log("R2城镇生活污水处理达标率:" + r2);
    console.log("R3乡村生活污水处理率（%）:" + r3);
    console.log("R4刷卡排污普及率(%):" + r4);

    console.log("E1吨水工业产出税收（元）:" + e1);
    console.log("E2单位排污权产出税收（万元/吨）:" + e2);

    var rd1 = 0
    if(d1 <= 200){
        rd1 = 1
    } else if (d1 < 1000) {
        rd1 = (1000 - d1) / (1000 - 200)
    } else {
        rd1 = 0
    }

    var rd2 = 0
    if(d2 >= 0.5) {
        rd2 = 1
    } else if(d2 > 0) {
        rd2 = d2 / 0.5
    } else {
        rd2 = 0
    }

    var rd3 = 0
    if(d3 >= 30){
        rd3 = 1
    } else if(d3 > 5){
        rd3 = (d3 - 5) / (30 - 5)
    } else {
        rd3 = 0
    }

    var rp1 = 0
    if(p1 <= 50){
        rp1 = 1
    } else if(p1 < 200){
        rp1 = (200 - p1) / (200 - 50)
    } else {
        rp1 = 0
    }

    var rp2 = 0
    if (p2 <= 225) {
        rp2 = 1
    } else if (p2 < 500) {
        rp2 = (500 - p2) / (500 - 225)
    } else {
        rp2 = 0
    }

    var rp3 = 0
    if (p3 <= 1) {
        rp3 = 1
    } else if(p3 < 10) {
        rp3 = (10 - p3) / (10 - 1)
    } else {
        rp3 = 0
    }

    var rs1 = 0
    if (s1 == 100) {
        rs1 = 1
    } else if (s1 > 50) {
        rs1 = (s1 - 50) / (100 - 50)
    } else {
        rs1 = 0
    }

    var rs2 = 0
    if (s2 <= 10) {
        rs2 = 1
    } else if(s2 < 100) {
        rs2 = (100 - s2) / (100 - 10)
    } else {
        rs2 = 0
    }

    var rs3 = 0
    if (s31 >= s32) {
        rs3 = 1
    } else if(s31 > s33) {
        rs3 = (s31 - s33) / (s32 - s33)
    } else {
        rs3 = 0
    }

    var rs4 = 0
    if(s4 >= 60) {
        rs4 = 1
    } else if(s4 > 18) {
        rs4 = (s4 - 18) / (60 -18)
    } else {
        rs4 = 0
    }

    var rr1 = 0
    if (r1 >= 10) {
        rr1 = 1
    } else if (r1 > 0) {
        rr1 = r1 / 10
    } else {
        rr1 = 0
    }

    var rr2 = 0
    if(r2 >= 100) {
        rr2 = 1
    } else if (r2 > 50) {
        rr2 = (r2 - 50) / (100 - 50)
    } else {
        rr2 = 0
    }

    var rr3 = 0
    if (r3 >= 100) {
        rr3 = 1
    } else if(r3 > 30) {
        rr3 = (r3 - 30) / (100 - 30)
    } else {
        rr3 = 0
    }

    var rr4 = 0
    if(r4 >= 100) {
        rr4 = 1
    } else if (r4 > 0){
        rr4 = r4 / 100
    } else {
        rr4 = 0
    }

    var re1 = 0
    if (e1 >= 100) {
        re1 = 1
    } else if(e1 > 20) {
        re1 = (e1 - 20) / (100 - 20)
    } else {
        re1 = 0
    }

    var re2 = 0
    if(e2 >= 300) {
        re2 = 1
    } else if(e2 > 20) {
        re2 = (e1 - 20) / (300 - 20)
    } else {
        re2 = 0
    }

    var dpsre_d = rd1 * 0.013 + rd2 * 0.026 + rd3 * 0.026
    var dpsre_p = rp1 * 0.020 + rp2 * 0.036 + rp3 * 0.065
    var dpsre_s = rs1 * 0.143 + rs2 * 0.041 + rs3 * 0.076 + rs4 * 0.076
    var dpsre_r = rr1 * 0.027 + rr2 * 0.108 + rr3 * 0.078 + rr4 * 0.068
    var dpsre_e = re1 * 0.079 + re2 * 0.118
    
    var dpsre = dpsre_d + dpsre_p + dpsre_s + dpsre_r + dpsre_e

    console.log(dpsre);

    var srelm_s = rd1 * 0.016 + rd2 * 0.027 + re2 * 0.045
    var srelm_r = rp1 * 0.025 + rp3 * 0.041 + rs2 * 0.094 + rs3 * 0.026 + re1 * 0.071
    var srelm_e = rs1 * 0.207 + rr2 * 0.124 + rr3 * 0.083
    var srelm_l = rd3 * 0.062 + rp2 * 0.031 + rs4 * 0.062
    var srelm_m = rr2 * 0.035 + rr4 * 0.053

    var srelm = srelm_s + srelm_r + srelm_e + srelm_l + srelm_m

    console.log(srelm);

    decorateCoreResult(sur_wecc, dpsre, srelm)

    var data = [
      {
        'name': '人口密度(人/km2)',
        'data': d1,
        'final-data': rd1
      },
      {
        'name': '单位国土面积地区生产总值(亿元/km2)',
        'data': d2,
        'final-data': rd2
      },
      {
        'name': '单位工业用地工业产出税收(万元/亩)',
        'data': d3,
        'final-data': rd3
      },
      {
        'name': '万元地区生产总值用水量(m3)',
        'data': p1,
        'final-data': rp1
      },
      {
        'name': '单位耕地面积化肥施用量(折纯)(kg/hm2)',
        'data': p2,
        'final-data': rp2
      },
      {
        'name': '单位工业产值废水排放量(m3/万元)',
        'data': p3,
        'final-data': rp3
      },
      {
        'name': 'S1区域出境断面水质达标率（%）',
        'data': s1,
        'final-data': rs1
      },
      {
        'name': '水资源开发利用率（%）',
        'data': s2,
        'final-data': rs2
      },
      {
        'name': '控制断面平均流量（%）',
        'data': s3,
        'final-data': rs3
      },
      {
        'name': '区域林草覆盖率（%）',
        'data': s4,
        'final-data': rs4
      },
      {
        'name': '水环境监管能力（万人环保专职人员数）(人)',
        'data': r1,
        'final-data': rr1
      },
      {
        'name': '城镇生活污水处理达标率',
        'data': r2,
        'final-data': rr2
      },
      {
        'name': '乡村生活污水处理率（%）',
        'data': r3,
        'final-data': rr3
      },
      {
        'name': '刷卡排污普及率(%)',
        'data': r4,
        'final-data': rr4
      },
      {
        'name': '吨水工业产出税收（元）',
        'data': e1,
        'final-data': re1
      },
      {
        'name': '单位排污权产出税收（万元/吨）',
        'data': e2,
        'final-data': re2
      }
    ]

    $('#sub-indicator1-table').bootstrapTable({data: data});


    var data2 = [
        {
          'name': '驱动力指数(D)',
          'data': dpsre_d
        },
        {
          'name': '压力指数(P)',
          'data': dpsre_p
        },
        {
          'name': '状态指数(S)',
          'data': dpsre_s
        },
        {
          'name': '响应指数(R)',
          'data': dpsre_r
        },
        {
          'name': '效益指数(E)',
          'data': dpsre_e
        }
    ]
    $('#sub-indicator2-table').bootstrapTable({data: data2});

    var data3 = [
        {
          'name': '社会经济指数(S)',
          'data': srelm_s
        },
        {
          'name': '水资源指数(R)',
          'data': srelm_r
        },
        {
          'name': '水生态环境指数(E)',
          'data': srelm_e
        },
        {
          'name': '土地功能指数(L)',
          'data': srelm_l
        },
        {
          'name': '水管理指数(M)',
          'data': srelm_m
        }
    ]
    $('#sub-indicator3-table').bootstrapTable({data: data3});

    $(".sub-indicator-table-div .table").addClass("d-none");
    $("#sub-indicator1-table").removeClass("d-none");
});

$("#prev2").click(function(){
    $('#state-tab').tab('show');
});

$("#return").click(function(){
    $("#reset1").click();
    $("#reset2").click();
    $('#quantity-tab').tab('show');
});

function decorateCoreResult(surWecc, dpsre, srelm){
    $("#wecc_result").removeClass();
    $("#dpsre_result").removeClass();
    $("#srelm_result").removeClass();

    if(surWecc == 0) {
        $("#wecc_result").html("临界超载")
        $("#wecc_result").addClass("text-warning");
    } else if(surWecc < 0) {
        $("#wecc_result").html("超载")
        $("#wecc_result").addClass("text-danger");
    } else {
        $("#wecc_result").html("未超载")
        $("#wecc_result").addClass("text-primary");
    }
    $("#wecc_result_number").text(surWecc.toFixed(3));

    if(dpsre < 0.5) {
        $("#dpsre_result").html("超载")
        $("#dpsre_result").addClass("text-danger");
    } else if(dpsre < 0.7) {
        $("#dpsre_result").html("临界超载")
        $("#dpsre_result").addClass("text-warning");
    } else {
        $("#dpsre_result").html("未超载")
        $("#dpsre_result").addClass("text-primary");
    }
    $("#dpsre_result_number").text(dpsre.toFixed(3));

    if(srelm < 0.5) {
        $("#srelm_result").html("超载")
        $("#srelm_result").addClass("text-danger");
    } else if(srelm < 0.7) {
        $("#srelm_result").html("临界超载")
        $("#srelm_result").addClass("text-warning");
    } else {
        $("#srelm_result").html("未超载")
        $("#srelm_result").addClass("text-primary");
    }
    $("#srelm_result_number").text(srelm.toFixed(3));
}

$(".wecc .btn").click(function(){
    $(".sub-indicator-table-div .table").addClass("d-none");
    $("#sub-indicator1-table").removeClass("d-none");
});

$(".dpsre .btn").click(function(){
    $(".sub-indicator-table-div .table").addClass("d-none");
    $("#sub-indicator2-table").removeClass("d-none");
});

$(".srelm .btn").click(function(){
    $(".sub-indicator-table-div .table").addClass("d-none");
    $("#sub-indicator3-table").removeClass("d-none");
});